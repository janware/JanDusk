using System;
using System.Drawing;
using System.Text;
using Microsoft.Win32;

namespace JanDusk
{
    class JanDuskSaver : Screensaver
    {
        double duskDelta;
        Image wallPaper;
#if DEBUG
        DateTime start;
        DateTime end;
#endif

        public JanDuskSaver() : base(FullscreenMode.SingleWindow)
        {
            Initialize += JanDuskSaver_Initialize;
            Update += JanDuskSaver_Update;
        }

        [STAThread]
        static void Main()
        {
            var jds = new JanDuskSaver();
            jds.Run();
        }

        private void JanDuskSaver_Update(object sender, EventArgs e)
        {
            if(ScreenSaverConfigForm.JanDuskMode == ScreenSaverConfigForm.DuskMode.GettingDark)
            {
                if(Window0.Form.Opacity >= 0)
                    Window0.Form.Opacity = Math.Max(0, Window0.Form.Opacity - DuskStep);
#if DEBUG
                if (Window0.Form.Opacity == 0) {
                    end = DateTime.Now;
                    MessageBox.Show($"That took {(end - start).TotalMinutes} minutes");
                    Window0.Form.Close();
                }
#endif
            }
            if(ScreenSaverConfigForm.JanDuskMode == ScreenSaverConfigForm.DuskMode.DuskAndDawn)
            {
                Window0.Form.Opacity += duskDelta;
                if(Window0.Form.Opacity >= 0.90)
                    duskDelta = -DuskStep;
                if(Window0.Form.Opacity <= 0.10)
                    duskDelta = DuskStep;
            }
        }



        private double DuskStep
        {
            get
            {
                double step = 0;
                switch(ScreenSaverConfigForm.JanDuskSpeed)
                {
                    case ScreenSaverConfigForm.Speed.Fast:
                        step = 0.0005;
                        break;
                    case ScreenSaverConfigForm.Speed.Medium:
                        step = 0.00004;
                        break;
                    case ScreenSaverConfigForm.Speed.Slow:
                        step = 0.00001;
                        break;
                }
                return step;
            }
        }

        private void JanDuskSaver_Initialize(object sender, EventArgs e)
        {
            duskDelta = DuskStep;
            var path = (byte[])Registry.CurrentUser.OpenSubKey("Control Panel\\Desktop").GetValue("TranscodedImageCache");
            var wallpaper_file = Encoding.Unicode.GetString(SliceMe(path, 24)).TrimEnd("\0".ToCharArray());
            wallPaper = Image.FromFile(wallpaper_file);

            ScreenSaverConfigForm.LoadSettings();
            Graphics0.DrawImage(wallPaper, 0, 0);

            if(ScreenSaverConfigForm.JanDuskMode == ScreenSaverConfigForm.DuskMode.OnlyTwilight)
            {
                Window0.Form.Opacity = 0.5;
            }
            else
            {
                Window0.Form.Opacity = 0.99;
            }

#if DEBUG
            start = DateTime.Now;
#endif

        }

        // Source: http://stackoverflow.com/a/406576/441907
        static byte[] SliceMe(byte[] source, int pos)
        {
            var destfoo = new byte[source.Length - pos];
            Array.Copy(source, pos, destfoo, 0, destfoo.Length);
            return destfoo;
        }

    }
}
