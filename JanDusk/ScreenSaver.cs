using System;
using System.Collections;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace JanDusk
{
    // This ScreenSaver class has been developed by Rei Miyasaka
    // source: https://www.codeproject.com/Articles/14081/Write-a-Screensaver-that-Actually-Works
    public abstract class Screensaver
    {
        protected Screensaver(FullscreenMode fullscreenMode)
        {
            this.fullscreenMode = fullscreenMode;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Framerate = 30;

            framerateTimer.Elapsed += new System.Timers.ElapsedEventHandler(FramerateTimer_Elapsed);
            framerateTimer.Start();
        }

        protected Screensaver()
            : this(FullscreenMode.MultipleWindows)
        {
        }

        void FramerateTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            updatesThisSec = 0;
            OneSecondTick?.Invoke(this, new EventArgs());
        }

        public event EventHandler Exit;

        [DllImport("winmm.dll")]
        static extern int TimeSetEvent(int delay, int resolution, TimeCallback callback, int user, int mode);

        [DllImport("winmm.dll")]
        static extern int TimeKillEvent(int id);

        [DllImport("winmm.dll")]
        static extern int TimeGetTime();

        delegate void TimeCallback(uint id, uint msg, IntPtr user, IntPtr param1, IntPtr param2);

        TimeCallback timerCallback;

        int timerId;

        void StartUpdating()
        {
            timerCallback = new TimeCallback(TimerCallback);
            timerId = TimeSetEvent((int)(1000 / (double)framerate), 0, timerCallback, 0, 0x0101);

            while(timerCallback != null)
            {
                updateEvent.WaitOne();
                DoUpdate();
                Application.DoEvents();
                updateEvent.Reset();
            }

        }

        void StopUpdating()
        {
            timerCallback = null;
            TimeKillEvent(timerId);
            updateEvent.WaitOne();
        }

        System.Threading.ManualResetEvent updateEvent = new System.Threading.ManualResetEvent(false);

        void TimerCallback(uint id, uint msg, IntPtr user, IntPtr param1, IntPtr param2) => updateEvent.Set();

        System.Timers.Timer framerateTimer = new System.Timers.Timer(1000);

        public event EventHandler OneSecondTick;

        int framerate;

        public int Framerate
        {
            get => framerate;
            set
            {
                if(value < 0)
                    throw new ArgumentOutOfRangeException();
                if(timerCallback != null)
                {
                    StopUpdating();
                    framerate = value;
                    StartUpdating();
                }
                else
                    framerate = value;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        struct RECT
        {
            public int left, top, right, bottom;
        }

        [DllImport("user32.dll")]
        static extern bool GetClientRect(IntPtr handle, out RECT rect);
        [DllImport("user32.dll")]
        static extern bool IsWindowVisible(IntPtr handle);

        static Rectangle GetClientRect(IntPtr handle)
        {
            GetClientRect(handle, out var rect);
            return Rectangle.FromLTRB(rect.left, rect.top, rect.right, rect.bottom);
        }

        public event EventHandler Update;

        event EventHandler PreUpdate;
        event EventHandler PostUpdate;

        int updatesThisSec;

        void DoUpdate()
        {
            if(screensaverMode == ScreensaverMode.Preview && !IsWindowVisible(windowHandle))
            {
                StopUpdating();
                Exit?.Invoke(this, new EventArgs());
                previewShutdownEvent.Set();
                return;
            }

            PreUpdate?.Invoke(this, new EventArgs());
            Update?.Invoke(this, new EventArgs());
            PostUpdate?.Invoke(this, new EventArgs());
            updatesThisSec++;
        }

        ScreensaverMode ProcessCommandLine()
        {
            var args = Environment.GetCommandLineArgs();

            if(args.Length == 1 && IsScr)
                return ScreensaverMode.Settings;

            if(args.Length < 2)
                throw new FormatException();

            if(args[1].ToLower().StartsWith("/c"))
            {
                return ScreensaverMode.Settings;
            }

            switch(args[1].ToLower())
            {
                case "w":
                    return ScreensaverMode.Windowed;
                case "/s":
                    return ScreensaverMode.Normal;
                case "/p":
                    if(args.Length < 3)
                    {
                        throw new FormatException();
                    }
                    try
                    {
                        windowHandle = (IntPtr)uint.Parse(args[2]);
                        return ScreensaverMode.Preview;
                    }
                    catch(FormatException)
                    {
                        throw new FormatException();
                    }
                default:
                    throw new FormatException();
            }
        }

        bool IsScr => System.IO.Path.GetExtension(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).
                    Equals(".scr", StringComparison.InvariantCultureIgnoreCase);

        public void Run() => Run(ScreensaverMode.Windowed);

        public void Run(ScreensaverMode mode)
        {
            if(mode == ScreensaverMode.Preview && windowHandle == IntPtr.Zero)
                throw new ArgumentException("Cannot explicity run in preview mode", "mode");

            if(isEnded)
                throw new Exception("This screensaver has already finished running");

            try
            {
                screensaverMode = ProcessCommandLine();
            }
            catch(FormatException)
            {
                screensaverMode = mode;
            }

            try
            {
                switch(screensaverMode)
                {
                    case ScreensaverMode.Windowed:
                        RunWindowed();
                        break;
                    case ScreensaverMode.Settings:
                        Application.Run(new ScreenSaverConfigForm());
                        break;
                    case ScreensaverMode.Normal:
                        RunNormal();
                        break;
                    case ScreensaverMode.Preview:
                        RunPreview();
                        break;
                }
            }
            finally
            {
                isEnded = true;
            }

        }

        FullscreenMode fullscreenMode = FullscreenMode.SingleWindow;
        ScreensaverMode screensaverMode;

        public ScreensaverMode Mode => screensaverMode;

        IntPtr windowHandle = IntPtr.Zero;

        public event EventHandler Initialize;

        WindowCollection windows;

        public WindowCollection Windows => windows;

        Window window0;

        public Window Window0
        {
            get
            {
                if(window0 != null)
                    return window0;

                if(windows == null || windows.Count == 0)
                    return null;

                window0 = windows[0];

                return window0;
            }
        }

        Graphics graphics0;

        public Graphics Graphics0
        {
            get
            {
                if(graphics0 != null)
                    return graphics0;

                if(Window0 == null)
                    return null;

                graphics0 = Window0.Graphics;
                return graphics0;
            }
        }

        System.Threading.AutoResetEvent previewShutdownEvent = new System.Threading.AutoResetEvent(false);

        private void RunPreview()
        {
#if DEBUG
            System.Diagnostics.Debugger.Launch();
#endif
            windows = new WindowCollection(new Window[] { new Window(this, windowHandle) });
            InitializeAndStart();
            previewShutdownEvent.WaitOne();
        }

        private void RunNormal()
        {
            Cursor.Hide();
            switch(fullscreenMode)
            {
                case FullscreenMode.SingleWindow:
                    RunNormalSingleWindow();
                    break;
                case FullscreenMode.MultipleWindows:
                    RunNormalMultipleWindows();
                    break;
            }
        }

        private void RunNormalMultipleWindows()
        {
            var windows = new ArrayList();

            var primary = new Form
            {
                StartPosition = FormStartPosition.Manual,
                Location = Screen.PrimaryScreen.Bounds.Location,
                Size = Screen.PrimaryScreen.Bounds.Size,
                BackColor = Color.Black,
#if !DEBUG
                TopMost = true,
#endif
                FormBorderStyle = FormBorderStyle.None,
                Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
            };

            foreach(var screen in Screen.AllScreens)
            {
                if(screen == Screen.PrimaryScreen)
                    continue;

                var form = new Form
                {
                    Owner = primary,
                    BackColor = Color.Black,
#if !DEBUG
                    TopMost = true,
#endif
                    StartPosition = FormStartPosition.Manual,
                    Location = screen.Bounds.Location,
                    Size = screen.Bounds.Size,
                    FormBorderStyle = FormBorderStyle.None,
                    Text = primary.Text
                };

                windows.Add(new Window(this, form));
            }

            windows.Insert(0, new Window(this, primary));

            primary.Load += delegate (object sender, EventArgs e)
            {
                foreach(Window window in this.windows)
                {
                    if(window.Form.Owner == null)
                        continue;
                    window.Form.Show();
                }
            };

            this.windows = new WindowCollection(windows.ToArray(typeof(Window)) as Window[]);

            primary.Show();
            InitializeAndStart();
        }

        private void RunNormalSingleWindow()
        {
            var form = new Form();
            var rect = GetVirtualScreenRect();
            form.Location = rect.Location;
            form.Size = rect.Size;
            form.BackColor = Color.Black;
#if !DEBUG
            form.TopMost = true;
#endif
            form.FormBorderStyle = FormBorderStyle.None;
            form.StartPosition = FormStartPosition.Manual;
            form.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;

            windows = new WindowCollection(new Window[] { new Window(this, form) });

            form.Show();
            InitializeAndStart();
        }

        static Rectangle GetVirtualScreenRect()
        {
            var screens = Screen.AllScreens;
            var rect = Rectangle.Empty;
            foreach(var screen in Screen.AllScreens)
                rect = Rectangle.Union(rect, screen.Bounds);
            return rect;
        }

        private void RunWindowed()
        {

            var form = new Form
            {
                FormBorderStyle = FormBorderStyle.FixedSingle,
                Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name,
                StartPosition = FormStartPosition.CenterScreen,
                BackColor = Color.Black,
#if !DEBUG
                TopMost = true,
#endif
                MaximizeBox = false,
                ClientSize = new Size((int)(Screen.PrimaryScreen.WorkingArea.Width * 0.9), (int)(Screen.PrimaryScreen.WorkingArea.Height * 0.9))
            };

            windows = new WindowCollection(new Window[] { new Window(this, form) });

            form.Show();
            InitializeAndStart();
        }

        void InitializeAndStart()
        {
            Initialize?.Invoke(this, new EventArgs());

            if(Window0 != null && Window0.Form != null)
                Window0.Form.FormClosing += new FormClosingEventHandler(Form_FormClosing);

            StartUpdating();
        }

        void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopUpdating();
            Exit?.Invoke(this, new EventArgs());

            e.Cancel = false;
        }

        bool isEnded = false;

        void OnMouseMove()
        {
            if(Window0.Form != null)
                Window0.Form.Close();
            else
                Application.Exit();
        }

        void OnKeyboardInput()
        {
            if(Window0.Form != null)
                Window0.Form.Close();
            else
                Application.Exit();
        }

        void OnMouseClick()
        {
            if(Window0.Form != null)
                Window0.Form.Close();
            else
                Application.Exit();
        }

        public class Window
        {
            internal Window(Screensaver screensaver, Form form)
            {
                this.screensaver = screensaver;
                this.form = form;
                size = form.ClientSize;
                graphics = form.CreateGraphics();
                handle = form.Handle;

                form.MouseMove += new MouseEventHandler(Form_MouseMove);
                form.MouseClick += new MouseEventHandler(Form_MouseClick);
                form.MouseDoubleClick += new MouseEventHandler(Form_MouseDoubleClick);
                form.MouseDown += new MouseEventHandler(Form_MouseDown);
                form.MouseUp += new MouseEventHandler(Form_MouseUp);
                form.MouseWheel += new MouseEventHandler(Form_MouseWheel);

                form.KeyDown += new KeyEventHandler(Form_KeyDown);
                form.KeyUp += new KeyEventHandler(Form_KeyUp);
                form.KeyPress += new KeyPressEventHandler(Form_KeyPress);

                this.screensaver.PreUpdate += new EventHandler(Screensaver_PreUpdate);
                this.screensaver.PostUpdate += new EventHandler(Screensaver_PostUpdate);
            }

            internal Window(Screensaver screensaver, IntPtr handle)
            {
                this.screensaver = screensaver;
                this.handle = handle;
                graphics = Graphics.FromHwnd(handle);
                size = GetClientRect(handle).Size;

                this.screensaver.PreUpdate += new EventHandler(Screensaver_PreUpdate);
                this.screensaver.PostUpdate += new EventHandler(Screensaver_PostUpdate);
            }

            bool doubleBuffer = false;
            bool doubleBufferSet = false;

            public bool DoubleBuffer
            {
                get
                {
                    if(!doubleBufferSet)
                        DoubleBuffer = true;
                    return doubleBuffer;
                }
                set
                {
                    doubleBufferSet = true;
                    if(doubleBuffer != value)
                    {
                        doubleBuffer = value;
                        if(doubleBuffer)
                            SetDoubleBuffer();
                        else
                            UnsetDoubleBuffer();
                    }
                    else
                        doubleBuffer = value;
                }
            }

            private void SetDoubleBuffer()
            {
                graphicsSwap = graphics;
                BufferedGraphicsManager.Current.MaximumBuffer = Size;
                buffer = BufferedGraphicsManager.Current.Allocate(graphicsSwap, new Rectangle(0, 0, Size.Width, Size.Height));
                graphics = buffer.Graphics;
            }

            private void UnsetDoubleBuffer()
            {
                buffer.Dispose();
                graphics = graphicsSwap;
                buffer = null;
                graphicsSwap = null;
            }

            BufferedGraphics buffer;
            Graphics graphicsSwap;

            void Screensaver_PreUpdate(object sender, EventArgs e)
            {
            }

            void Screensaver_PostUpdate(object sender, EventArgs e)
            {
                if(doubleBuffer)
                {
                    buffer.Render(graphicsSwap);
                }
            }

            void Form_KeyPress(object sender, KeyPressEventArgs e)
            {
                KeyPress?.Invoke(this, e);
                screensaver.OnKeyboardInput();
            }

            void Form_KeyUp(object sender, KeyEventArgs e)
            {
                KeyUp?.Invoke(this, e);
                screensaver.OnKeyboardInput();
            }

            void Form_KeyDown(object sender, KeyEventArgs e)
            {
                KeyDown?.Invoke(this, e);
                screensaver.OnKeyboardInput();
            }

            void Form_MouseWheel(object sender, MouseEventArgs e)
            {
                MouseWheel?.Invoke(this, e);
                screensaver.OnMouseClick();
            }

            void Form_MouseUp(object sender, MouseEventArgs e)
            {
                MouseUp?.Invoke(this, e);
                screensaver.OnMouseClick();
            }

            void Form_MouseDown(object sender, MouseEventArgs e)
            {
                MouseDown?.Invoke(this, e);
                screensaver.OnMouseClick();
            }

            void Form_MouseDoubleClick(object sender, MouseEventArgs e)
            {
                MouseDoubleClick?.Invoke(this, e);
                screensaver.OnMouseClick();
            }

            void Form_MouseClick(object sender, MouseEventArgs e)
            {
                MouseClick?.Invoke(this, e);
                screensaver.OnMouseClick();
            }

            Point mousePosition = Point.Empty;

            void Form_MouseMove(object sender, MouseEventArgs e)
            {
                MouseMove?.Invoke(this, e);

                if(mousePosition == Point.Empty)
                    mousePosition = e.Location;
                else if(mousePosition != e.Location)
                    screensaver.OnMouseMove();
            }

            public event MouseEventHandler MouseMove;
            public event MouseEventHandler MouseClick;
            public event MouseEventHandler MouseDoubleClick;
            public event MouseEventHandler MouseWheel;
            public event MouseEventHandler MouseUp;
            public event MouseEventHandler MouseDown;

            public event KeyEventHandler KeyDown;
            public event KeyEventHandler KeyUp;
            public event KeyPressEventHandler KeyPress;

            Screensaver screensaver;

            public Screensaver Screensaver => screensaver;

            Form form;

            public Form Form => form;

            IntPtr handle;

            public IntPtr Handle => handle;

            Size size;

            public Size Size => size;

            Graphics graphics;

            public Graphics Graphics
            {
                get
                {
                    if(!doubleBufferSet)
                        DoubleBuffer = true;
                    return graphics;
                }
            }

            public Screen Screen => Screen.FromHandle(handle);
        }

        public class WindowCollection : IEnumerable
        {
            internal WindowCollection(Window[] windows) => this.windows = windows;

            Window[] windows;

            public Window this[int index] => windows[index];

            public int Count => windows.Length;

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() => windows.GetEnumerator();
        }
    }

    public enum FullscreenMode
    {
        SingleWindow,
        MultipleWindows
    }

    public enum ScreensaverMode
    {
        Settings,
        Preview,
        Normal,
        Windowed
    }
}
