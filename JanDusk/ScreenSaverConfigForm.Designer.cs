﻿namespace JanDusk
{
    partial class ScreenSaverConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScreenSaverConfigForm));
            this.rbGettingDark = new System.Windows.Forms.RadioButton();
            this.rbOnlyTwilight = new System.Windows.Forms.RadioButton();
            this.rbDuskAndDawn = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbFast = new System.Windows.Forms.RadioButton();
            this.rbSlow = new System.Windows.Forms.RadioButton();
            this.rbMedium = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbGettingDark
            // 
            this.rbGettingDark.AutoSize = true;
            this.rbGettingDark.Location = new System.Drawing.Point(8, 46);
            this.rbGettingDark.Name = "rbGettingDark";
            this.rbGettingDark.Size = new System.Drawing.Size(83, 17);
            this.rbGettingDark.TabIndex = 0;
            this.rbGettingDark.TabStop = true;
            this.rbGettingDark.Text = "Getting dark";
            this.rbGettingDark.UseVisualStyleBackColor = true;
            this.rbGettingDark.CheckedChanged += new System.EventHandler(this.RbGettingDark_CheckedChanged);
            // 
            // rbOnlyTwilight
            // 
            this.rbOnlyTwilight.AutoSize = true;
            this.rbOnlyTwilight.Location = new System.Drawing.Point(8, 23);
            this.rbOnlyTwilight.Name = "rbOnlyTwilight";
            this.rbOnlyTwilight.Size = new System.Drawing.Size(85, 17);
            this.rbOnlyTwilight.TabIndex = 1;
            this.rbOnlyTwilight.TabStop = true;
            this.rbOnlyTwilight.Text = "Only Twilight";
            this.rbOnlyTwilight.UseVisualStyleBackColor = true;
            this.rbOnlyTwilight.CheckedChanged += new System.EventHandler(this.RbOnlyTwilight_CheckedChanged);
            // 
            // rbDuskAndDawn
            // 
            this.rbDuskAndDawn.AutoSize = true;
            this.rbDuskAndDawn.Location = new System.Drawing.Point(8, 69);
            this.rbDuskAndDawn.Name = "rbDuskAndDawn";
            this.rbDuskAndDawn.Size = new System.Drawing.Size(102, 17);
            this.rbDuskAndDawn.TabIndex = 2;
            this.rbDuskAndDawn.TabStop = true;
            this.rbDuskAndDawn.Text = "Dusk and Dawn";
            this.rbDuskAndDawn.UseVisualStyleBackColor = true;
            this.rbDuskAndDawn.CheckedChanged += new System.EventHandler(this.RbDuskAndDawn_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbDuskAndDawn);
            this.groupBox1.Controls.Add(this.rbOnlyTwilight);
            this.groupBox1.Controls.Add(this.rbGettingDark);
            this.groupBox1.Location = new System.Drawing.Point(14, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(185, 101);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Twilight Mode";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbFast);
            this.groupBox2.Controls.Add(this.rbSlow);
            this.groupBox2.Controls.Add(this.rbMedium);
            this.groupBox2.Location = new System.Drawing.Point(14, 148);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(185, 101);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Speed";
            // 
            // rbFast
            // 
            this.rbFast.AutoSize = true;
            this.rbFast.Location = new System.Drawing.Point(8, 69);
            this.rbFast.Name = "rbFast";
            this.rbFast.Size = new System.Drawing.Size(144, 17);
            this.rbFast.TabIndex = 2;
            this.rbFast.TabStop = true;
            this.rbFast.Text = "Fast (1 min to 100% dark)";
            this.rbFast.UseVisualStyleBackColor = true;
            this.rbFast.CheckedChanged += new System.EventHandler(this.RbFast_CheckedChanged);
            // 
            // rbSlow
            // 
            this.rbSlow.AutoSize = true;
            this.rbSlow.Location = new System.Drawing.Point(8, 23);
            this.rbSlow.Name = "rbSlow";
            this.rbSlow.Size = new System.Drawing.Size(152, 17);
            this.rbSlow.TabIndex = 1;
            this.rbSlow.TabStop = true;
            this.rbSlow.Text = "Slow (1 hour to 100% dark)";
            this.rbSlow.UseVisualStyleBackColor = true;
            this.rbSlow.CheckedChanged += new System.EventHandler(this.RbSlow_CheckedChanged);
            // 
            // rbMedium
            // 
            this.rbMedium.AutoSize = true;
            this.rbMedium.Location = new System.Drawing.Point(8, 46);
            this.rbMedium.Name = "rbMedium";
            this.rbMedium.Size = new System.Drawing.Size(167, 17);
            this.rbMedium.TabIndex = 0;
            this.rbMedium.TabStop = true;
            this.rbMedium.Text = "Medium (10 min to 100% dark)";
            this.rbMedium.UseVisualStyleBackColor = true;
            this.rbMedium.CheckedChanged += new System.EventHandler(this.RbMedium_CheckedChanged);
            // 
            // ScreenSaverConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(222, 261);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ScreenSaverConfigForm";
            this.Text = "JanDusk Settings";
            this.Load += new System.EventHandler(this.ScreenSaverConfigForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton rbGettingDark;
        private System.Windows.Forms.RadioButton rbOnlyTwilight;
        private System.Windows.Forms.RadioButton rbDuskAndDawn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbFast;
        private System.Windows.Forms.RadioButton rbSlow;
        private System.Windows.Forms.RadioButton rbMedium;
    }
}