using System;
using System.Windows.Forms;
using Microsoft.Win32;

namespace JanDusk
{
    public partial class ScreenSaverConfigForm : Form
    {
        public ScreenSaverConfigForm() => InitializeComponent();

        private void ScreenSaverConfigForm_Load(object sender, EventArgs e)
        {
            LoadSettings();
            rbOnlyTwilight.Checked = (JanDuskMode == DuskMode.OnlyTwilight);
            rbGettingDark.Checked = (JanDuskMode == DuskMode.GettingDark);
            rbDuskAndDawn.Checked = (JanDuskMode == DuskMode.DuskAndDawn);
            rbSlow.Checked = (JanDuskSpeed == Speed.Slow);
            rbMedium.Checked = (JanDuskSpeed == Speed.Medium);
            rbFast.Checked = (JanDuskSpeed == Speed.Fast);

            if (rbOnlyTwilight.Checked) groupBox2.Enabled = false;
        }

        private static void SaveSettings()
        {
            var key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\JanDuskScreenSaver");

            key.SetValue("mode", (int)JanDuskMode);
            key.SetValue("speed", (int)JanDuskSpeed);
        }

        public static void LoadSettings()
        {
            var key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\JanDuskScreenSaver");
            if (key == null)
            {
                JanDuskMode = DuskMode.OnlyTwilight;
                JanDuskSpeed = Speed.Slow;
                SaveSettings();
            }
            else
            {
                JanDuskMode = (DuskMode)key.GetValue("mode");
                JanDuskSpeed = (Speed)key.GetValue("speed");
            }
        }

        public static DuskMode JanDuskMode;
        public static Speed JanDuskSpeed;

        public enum Speed
        {
            Slow,
            Medium,
            Fast
        }

        public enum DuskMode
        {
            OnlyTwilight,
            GettingDark,
            DuskAndDawn
        }

        private void RbOnlyTwilight_CheckedChanged(object sender, EventArgs e)
        {
            if (rbOnlyTwilight.Checked)
            {
                JanDuskMode = DuskMode.OnlyTwilight;
                groupBox2.Enabled = false;
            }
            else
            {
                groupBox2.Enabled = true;
            }
            SaveSettings();
        }

        private void RbGettingDark_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGettingDark.Checked)
            {
                JanDuskMode = DuskMode.GettingDark;
            }
            SaveSettings();
        }

        private void RbDuskAndDawn_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDuskAndDawn.Checked)
            {
                JanDuskMode = DuskMode.DuskAndDawn;
            }
            SaveSettings();
        }

        private void RbSlow_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSlow.Checked)
            {
                JanDuskSpeed = Speed.Slow;
            }
            SaveSettings();
        }

        private void RbMedium_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMedium.Checked)
            {
                JanDuskSpeed = Speed.Medium;
            }
            SaveSettings();
        }

        private void RbFast_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFast.Checked)
            {
                JanDuskSpeed = Speed.Fast;
            }
            SaveSettings();
        }
    }
}
