# Jan's Twilight ScreenSaver

## What the Screensaver does
The screen saver displays your wallpaper image and it then applies a twilight theme onto it.

You will get the best results when your wallpaper is a scenic photo.

It also works when you have multiple monitors.

## Configuration
![Alt text](/configscreen.png?raw=true "Configuration Screen")

The screen saver has three twilight modes:

| Mode | Description |
| --- | --- |
| Only Twilight | The picture is displayed as if dusk is half-way |
| Getting Dark | The picture is displayed with daylight, but it will get dark gradually. When it is completely dark the screen will stay black until the screen saver is terminated |
| Dusk and Dawn | After getting dark it will getting light again. Then the process starts all over again. |

You can define three speeds to choose how long the dark and light cycle will take.

## Installation
Download the jandusk.scr file from the dist folder, and copy it into your c:\windows\system32 folder.
Then go to your Screen Saver Settings, adjust the configuration, and you are ready to go.

### JanWare
Find my other software at [my JanWare website](http://www.janware.nl "JanWare")
